﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace DirectoryWatcher.Tests
{
    [TestClass]
    public class ParameterUseTest
    {
        [TestMethod]
        [TestCategory("JsonReader")]
        public void Empty_ReadsToNull()
        {
            //arrange
            var reader = new JsonReader();

            //act
            var result = reader.Read("");

            //assert
            Assert.IsNull(result);
        }

        [TestMethod]
        [TestCategory("JsonReader")]
        public void UnexpectedInput_Fails()
        {
            //arrange
            var reader = new JsonReader();

            //act
            try
            {
                reader.Read("{}}");
                Assert.Fail();
            }
            catch (JsonReaderException ex)
            {
                //assert
                Assert.AreEqual(1, ex.Line);
                Assert.AreEqual(2, ex.Position);
            }
        }

        [TestMethod]
        [TestCategory("JsonReader")]
        public void Integer_ReadsToInteger()
        {
            //arrange
            var reader = new JsonReader();

            //act
            var result = (int)reader.Read("5");

            //assert
            Assert.AreEqual(5, result);
        }

        [TestMethod]
        [TestCategory("JsonReader")]
        public void Float_ReadsToDouble()
        {
            //arrange
            var reader = new JsonReader();

            //act
            var result = (double)reader.Read("5.5");

            //assert
            Assert.AreEqual(5.5, result);
        }

        [TestMethod]
        [TestCategory("JsonReader")]
        public void String_ReadsToString()
        {
            //arrange
            var reader = new JsonReader();

            //act
            var result = (string)reader.Read("\"5\"");

            //assert
            Assert.AreEqual("5", result);
        }

        [TestMethod]
        [TestCategory("JsonReader")]
        public void Boolean_ReadsToBoolean()
        {
            //arrange
            var reader = new JsonReader();

            //act
            var result = (bool)reader.Read("true");

            //assert
            Assert.AreEqual(true, result);
        }

        [TestMethod]
        [TestCategory("JsonReader")]
        public void Date_ReadsToDate()
        {
            //arrange
            var reader = new JsonReader();

            //act
            var result = (DateTime)reader.Read("\"2016-01-05T18:25:43Z\"");

            //assert
            Assert.AreEqual(5, result.Day);
            Assert.AreEqual(1, result.Month);
            Assert.AreEqual(2016, result.Year);
            Assert.AreEqual(18, result.Hour);
            Assert.AreEqual(25, result.Minute);
            Assert.AreEqual(43, result.Second);
        }

        [TestMethod]
        [TestCategory("JsonReader")]
        public void Null_ReadsToNull()
        {
            //arrange
            var reader = new JsonReader();

            //act
            var result = reader.Read("null");

            //assert
            Assert.IsNull(result);
        }

        [TestMethod]
        [TestCategory("JsonReader")]
        public void Undefined_ReadsToNull()
        {
            //arrange
            var reader = new JsonReader();

            //act
            var result = reader.Read("undefined");

            //assert
            Assert.IsNull(result);
        }

        [TestMethod]
        [TestCategory("JsonReader")]
        public void Comment_IsOmitted()
        {
            //arrange
            var reader = new JsonReader();

            //act
            var result = reader.Read("{/*comment*/}") as Dictionary<string, object>;

            //assert
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        [TestCategory("JsonReader")]
        public void UnclosedEmptyObject_Fails()
        {
            //arrange
            var reader = new JsonReader();

            //act
            try
            {
                reader.Read("{");
                Assert.Fail();
            }
            catch (JsonReaderException ex)
            {
                //assert
                Assert.AreEqual(1, ex.Line);
                Assert.AreEqual(1, ex.Position);
                Assert.AreEqual("Unexpected end of JSON", ex.Message);
            }
        }

        [TestMethod]
        [TestCategory("JsonReader")]
        public void EmptyObject_ReadsNormally()
        {
            //arrange
            var reader = new JsonReader();

            //act
            var result = reader.Read("{}") as Dictionary<string, object>;

            //assert
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        [TestCategory("JsonReader")]
        public void Object_ReadsNormally()
        {
            //arrange
            var reader = new JsonReader();

            //act
            var result = reader.Read(@"{
                test:5,
                items:[
                    {a:1},
                    {b:2},
                    undefined,
                    [2,3],
                    false
                ],
                empty:null
            }") as Dictionary<string, object>;

            //assert
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(5, result["test"]);
            Assert.IsNull(result["empty"]);
            var items = result["items"] as List<object>;
            Assert.AreEqual(5, items.Count);
            Assert.AreEqual(1, (items[0] as Dictionary<string, object>).Count);
            Assert.AreEqual(1, (items[1] as Dictionary<string, object>).Count);
            Assert.IsNull(items[2]);
            Assert.AreEqual(2, (items[3] as List<object>).Count);
            Assert.AreEqual(false, items[4]);
        }

        [TestMethod]
        [TestCategory("JsonReader")]
        public void ObjectDuplicateName_Fails()
        {
            //arrange
            var reader = new JsonReader();

            //act
            try
            {
                reader.Read(@"{a:1,a:2}");
                Assert.Fail();
            }
            catch (JsonReaderException ex)
            {
                //assert
                Assert.AreEqual(1, ex.Line);
                Assert.AreEqual(7, ex.Position);
                Assert.AreEqual("Name `a` already used", ex.Message);
            }
        }

        [TestMethod]
        [TestCategory("JsonReader")]
        public void Array_ReadsNormally()
        {
            //arrange
            var reader = new JsonReader();

            //act
            var result = reader.Read(@"[
                5,
                {
                    one:{a:1},
                    two:{b:2},
                    three:undefined,
                    four:[2,3],
                    five:false
                },
                null
            ]") as List<object>;

            //assert
            Assert.AreEqual(3, result.Count);
            Assert.AreEqual(5, result[0]);
            Assert.IsNull(result[2]);
            var items = result[1] as Dictionary<string, object>;
            Assert.AreEqual(5, items.Count);
            Assert.AreEqual(1, (items["one"] as Dictionary<string, object>).Count);
            Assert.AreEqual(1, (items["two"] as Dictionary<string, object>).Count);
            Assert.IsNull(items["three"]);
            Assert.AreEqual(2, (items["four"] as List<object>).Count);
            Assert.AreEqual(false, items["five"]);
        }
    }
}