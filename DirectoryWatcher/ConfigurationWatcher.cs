﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace DirectoryWatcher
{
    internal class ConfigurationWatcher : Worker
    {
        public event EventHandler ConfigurationChanged;

        public event EventHandler SourceRulesChanged;

        private FileInfo File;
        private object Configuration;

        public ConfigurationWatcher(Service service, string name, string path)
            : base(service, name)
        {
            File = new FileInfo(path);
        }

        protected override void Run(CancellationToken token)
        {
            using (var watcher = new FileWatcher(File.FullName, TimeSpan.FromMilliseconds(200)))
            {
                Configuration = GetConfiguration();

                watcher.Changed += (sender, e) =>
                {
                    Debug.WriteLine("Configuration file changed");
                    HandleUpdate();
                };

                watcher.Removed += (sender, e) =>
                {
                    Debug.WriteLine("Configuration file removed");
                    Service.Shutdown();
                };

                //wait until cancelled
                token.WaitHandle.WaitOne();
                Debug.WriteLine("Shutting down configuration watcher");
            }
        }

        private void HandleUpdate()
        {
            //get actual configuration
            var configuration = GetConfiguration();
        }

        private object GetConfiguration()
        {
            using (var stream = File.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (var reader = new StreamReader(stream))
            {
                var content = reader.ReadToEnd();
                var raw = new JsonReader().Read(content);

                return new Configuration(raw as Dictionary<string, object>);
            }
        }
    }
}