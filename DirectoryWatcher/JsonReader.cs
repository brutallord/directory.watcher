﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace DirectoryWatcher
{
    public class JsonReader
    {
        private JsonTextReader Reader { get; set; }

        public object Read(string json)
        {
            Reader = new JsonTextReader(new StringReader(json));

            //return null if empty
            if (!Read())
                return null;

            var result = ReadToken();

            //call read (if eny unexpected input - it will fail, otherwise - simply returns false)
            Read();

            return result;
        }

        private object ReadToken()
        {
            var value = Reader.Value?.ToString();
            switch (Reader.TokenType)
            {
                case JsonToken.StartObject:
                    var objectToken = new Dictionary<string, object>();
                    while (true)
                    {
                        //if nothing to read - end was not expected
                        SafeRead();

                        //if object end - read is ended
                        if (Reader.TokenType == JsonToken.EndObject)
                            break;

                        var name = Reader.Value.ToString();

                        //if name already used - not expected
                        if (objectToken.ContainsKey(name))
                            throw new JsonReaderException(Reader.LineNumber, Reader.LinePosition, string.Format("Name `{0}` already used", name));

                        SafeRead();
                        objectToken.Add(name, ReadToken());
                    }

                    return objectToken;

                case JsonToken.StartArray:
                    var arrayToken = new List<object>();
                    while (true)
                    {
                        //if nothing to read - end was not expected
                        SafeRead();

                        //if array end - read is ended
                        if (Reader.TokenType == JsonToken.EndArray)
                            break;

                        arrayToken.Add(ReadToken());
                    }

                    return arrayToken;

                case JsonToken.Integer:
                    return int.Parse(value);

                case JsonToken.Float:
                    return double.Parse(value);

                case JsonToken.String:
                    return value;

                case JsonToken.Boolean:
                    return bool.Parse(value);

                case JsonToken.Date:
                    return DateTime.Parse(value);

                case JsonToken.Null:
                case JsonToken.Undefined:
                default:
                    return null;
            }
        }

        private bool Read()
        {
            try
            {
                return Reader.Read();
            }
            catch (Newtonsoft.Json.JsonReaderException ex)
            {
                throw new JsonReaderException(ex.LineNumber, ex.LinePosition, ex.Message);
            }
        }

        private void SafeRead()
        {
            bool read;
            while (read = Read())
                if (Reader.TokenType != JsonToken.Comment)
                    break;

            if (!read)
                throw new JsonReaderException(Reader.LineNumber, Reader.LinePosition, "Unexpected end of JSON");
        }
    }
}