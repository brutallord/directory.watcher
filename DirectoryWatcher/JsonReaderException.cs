﻿using System;

namespace DirectoryWatcher
{
    public class JsonReaderException : Exception
    {
        public int Line { get; }

        public int Position { get; }

        public JsonReaderException(int line, int position, string message)
            : base(message)
        {
            Line = line;
            Position = position;
        }
    }
}