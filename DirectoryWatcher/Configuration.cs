﻿using System.Collections.Generic;

namespace DirectoryWatcher
{
    /*
    1. read complex configuration object manually from json
    2. provide internally-comparable implementation, that returns changes flags
    3. this configuration + sources watcher initial read = first run configration for compiler
    4. this configuration changed = enqueue compiler upgrade with updated configuration
    5. sources changed = enqueue compiler upgrade with updated configuration
    */

    class Configuration
    {
        public Configuration(Dictionary<string, object> json)
        {
        }
    }
}